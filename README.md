# Hot R.O.D. - Rides on Demand

## Running the application locally.
1. Start a local Redis server with `docker run -p 6379:6379 redis`
2. Start a local Postgres server with `docker run -p 5432:5432 postgres`
3. Seed the Redis server with `go run ./scripts/seed_redis/seed_redis.go`
4. Seed the Postgres server with `go run ./scripts/seed_postgres/seed_postgres.go`
5. Build the code with `go install .`
6. Start the `frontend` service with  `hotrod frontend`.
7. Start the `customer` service with `hotrod customer`.
8. Start the `driver` service with `hotrod driver`.
9. Start the `route` service with `hotrod route`.
10. Go to `localhost:8080` in your browser.
    - There should four buttons.
    - Click on any of them.
    - It should say "Dispatching a car..."
    - Then "HotROD <LICENSE> arriving in 2min"
