import redis, sys

"""
Checks that server is up.
Checks that user has read and write permission.
Checks that seed data is in database.
"""
def test(host, port):
	client = redis.StrictRedis(host=host, port=port)
	client.ping()

	client.set("TestKey", "KeyValue")
	assert client.get("TestKey") == "KeyValue", "Cannot Set & Get"

	d = len(client.keys("T7*C"))
	assert d == 50, "{} instead of 50 drivers in Redis".format(d)

	return True


if __name__ == '__main__':
	host = sys.argv[1]
	port = int(sys.argv[2])
	test(host, port)