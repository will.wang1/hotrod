import requests

"""
Checks that the server returns a valid response
"""
def test(host, port):
	response = requests.get("http://" + host + ":" + str(port))
	assert response.status_code == 200, "Status code was {} instead of 200".format(response.status_code)

        payload = {'customer': '392', 'nonse': '0.5'}
	response = requests.get("http://" + host + ":" + str(port) + "/dispatch", params=payload).json()
	assert response["ETA"] == 120000000000, "Incorrect Response {}".format(response)
	print response

if __name__ == '__main__':
	host = sys.argv[1]
	port = int(sys.argv[2])
	test(host, port)
