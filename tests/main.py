import sys, subprocess, time
import redis_test, postgres_test, server_test

command = "kubectl get services | grep {} | awk '{{print $4}}'"

def is_ip(ipstr):
	s = ipstr.split(".")
	assert len(s) == 4, "IP must have 4 numbers: {}".format(ipstr)
	for i in s:
		assert int(i) <= 255 and int(i) >= 0, "Invalid numbers for IP: {}".format(ipstr)

def test(namespace):

	while True:
		value = subprocess.check_output(command.format("redis-master"), shell=True).strip()
		assert value != "<none>", "redis-master must have an external ip"
		if value == "<pending>":
			time.sleep(1)
		else:
			is_ip(value)
			break

	print "Testing Redis @", value, 6379
	redis_test.test(value, 6379)

	print "Testing Postgres @", "<pass>", "<pass>"
	postgres_test.test()

	while True:
		value = subprocess.check_output(command.format("hotrod"), shell=True).strip()
		assert value != "<none>", "hortod must have an external ip"
		if value == "<pending>":
			time.sleep(1)
		else:
			is_ip(value)
			break

	print "Testing Server @", value, 8080
	server_test.test(value, 8080)


if __name__ == '__main__':

	test(sys.argv[1])