FROM golang:1.8

EXPOSE 8080 8081 8082 8083

RUN mkdir -p /go/src/github.com/kelda/hotrod
RUN mkdir -p /go/bin
RUN go get github.com/Masterminds/glide
WORKDIR /go/src/github.com/kelda/hotrod
COPY glide.* ./
RUN glide install
RUN go get github.com/go-redis/redis
RUN go get github.com/lib/pq
RUN go get github.com/sirupsen/logrus

COPY . .

RUN go build -o hotrod main.go 
RUN mv hotrod /go/bin/
ENTRYPOINT ["/go/bin/hotrod", "all"]

